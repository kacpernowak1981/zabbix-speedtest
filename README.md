# Zabbix Speedtest
Inspired by https://git.cdp.li/polcape/zabbix/tree/master/zabbix-speedtest

In this method the Zabbix server gets the value from the agent

Monitoring the speedtest by different ISP server


# How is work

In the speedtest.sh file, at the top there are tables with the IDs of the servers on which the speedtest was performed. Modify them depending on your needs.

On the agent, the speedtest.sh file runs every 10 minutes and as a result, it fills some files (one for each server ID plus one general one for the top server) in the /tmp/ directory.
The server receives this result every 600 seconds and creates graphs.

# Install instruction
- Install official Ookla speedtest package, instruction available here:
  https://www.speedtest.net/apps/cli
- Install jq package ex. apt install jq (yum insall jq)
- Copy the `speedtest.sh` to `/etc/zabbix/script` or execute commands:
`sudo mkdir /etc/zabbix/script && cp speedtest.sh /etc/zabbix/script/`
`chmod +x /etc/zabbix/script/speedtest.sh`
- Alternative 1 (recommended): execute command:
`sudo cat "speedtest.cron" >> to /etc/crontab`
- Alternative 2 (not tested): Install the systemd service and timer: `cp speedtest.service speedtest.timer /etc/systemd/system` and start and enable the timer: `systemctl enable --now speedtest.timer`
- Import the zabbix-agent config: `cp speedtest.conf /etc/zabbix/zabbix_agentd.conf.d/` or execute command:
`sudo cat "speedtest.conf" > /etc/zabbix/zabbix_agentd.conf.d/speedtest.conf`
- Restart zabbix-agent: `systemctl restart zabbix-agent`
- Import `speedtest_zabbix_template.xml` on your Zabbix server

# Changes to the source
The source project uses the `speedtest-cli` package, which allows speed testing only against servers returned by the `spectest-cli --list` command.
This command returns a list of different servers over a period of time. The change is to replace `spectest-cli` with the official `speedtest' package from Ookla, which does not have the mentioned limitations.